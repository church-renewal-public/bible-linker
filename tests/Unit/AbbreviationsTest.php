<?php

use ChurchRenewal\BibleLinker\Abbreviations;
use ChurchRenewal\BibleLinker\BibleLinker;
use PHPUnit\Framework\TestCase;

final class AbbreviationsTest extends TestCase {
    /** make sure all abbreviations are match-able by our regex */
    function testAllAbbreviations() {
        $languages = ['de', 'en', 'es', 'fr', 'pt', 'ru', 'uk'];

        foreach ($languages as $langId) {
            $invalid = [];
            $regex = BibleLinker::bookNameRegex($langId);
            foreach (Abbreviations::get($langId) as $key => $abbreviations) {
                foreach ($abbreviations as $abbreviation) {
                    $match = [];
                    $verse = "$abbreviation 1:1";
                    preg_match($regex, $verse, $match);
                    if (empty($match)) $invalid[] = $abbreviation;
                }
            }

            $this->assertEmpty($invalid, 'The following abbreviations are invalid for language '. $langId .': ['.implode(',', $invalid).']');
        }
    }

    function testAllAbbreviationsWithPeriods() {
        $languages = ['de', 'en', 'es', 'fr', 'pt', 'ru', 'uk'];

        foreach ($languages as $langId) {
            $invalid = [];
            $regex = BibleLinker::bookNameRegex($langId);
            foreach (Abbreviations::get($langId) as $key => $abbreviations) {
                foreach ($abbreviations as $abbreviation) {
                    $match = [];
                    $verse = "$abbreviation. 1:1";
                    preg_match($regex, $verse, $match);
                    if (empty($match)) $invalid[] = $abbreviation;
                }
            }

            $this->assertEmpty($invalid, 'The following abbreviations are invalid for language '. $langId .': ['.implode(',', $invalid).']');
        }
    }
}