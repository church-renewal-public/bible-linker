<?php

use ChurchRenewal\BibleLinker\BibleLinker;
use PHPUnit\Framework\TestCase;

final class BibleLinkerTest extends TestCase {
	function testSingleVersesEnglish() {
		$linkifiedContent = BibleLinker::linkify("Psalm 119:105", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/PSA.119.105.NIV">Psalm 119:105</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testSingleVersesSpanish() {
		$linkifiedContent = BibleLinker::linkify("Salmos 119:105", "es");
		$this->assertEquals(
			'<a href="https://www.bible.com/es/bible/149/PSA.119.105.RVR1960">Salmos 119:105</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testSingleVersesFrench() {
		$linkifiedContent = BibleLinker::linkify("Psaumes 119:105", "fr");
		$this->assertEquals(
			'<a href="https://www.bible.com/fr/bible/93/PSA.119.105.LSG">Psaumes 119:105</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testSingleVersesPortuguese() {
		$linkifiedContent = BibleLinker::linkify("Salmos 119:105", "pt");
		$this->assertEquals(
			'<a href="https://www.bible.com/pt/bible/129/PSA.119.105.NVI">Salmos 119:105</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testSingleVersesGerman() {
		$linkifiedContent = BibleLinker::linkify("Psalmen 119:105", "de");
		$this->assertEquals(
			'<a href="https://www.bible.com/de/bible/157/PSA.119.105.SCH2000">Psalmen 119:105</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testSingleVersesRussian() {
		$linkifiedContent = BibleLinker::linkify("Псалтирь 119:105", "ru");
		$this->assertEquals(
			'<a href="https://www.bible.com/ru/bible/143/PSA.119.105.' . urlencode("НРП") . '">Псалтирь 119:105</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testSingleVersesUkrainian() {
		$linkifiedContent = BibleLinker::linkify("Псалми 119:105", "uk");
		$this->assertEquals(
			'<a href="https://www.bible.com/uk/bible/204/PSA.119.105.UMT">Псалми 119:105</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testLanguageNotSupported() {
		$linkifiedContent = BibleLinker::linkify("Psalm 119:105", "io");

		$this->assertEquals($linkifiedContent["new"], 0);
		$this->assertEquals($linkifiedContent["alternative"], 0);
		$this->assertEquals($linkifiedContent["unknown"], 0);
		$this->assertEquals($linkifiedContent["unknownTranslations"], []);
		$this->assertEquals($linkifiedContent["html"], "Psalm 119:105");
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testNoBooksFound() {
		$linkifiedContent = BibleLinker::linkify("The quick brown fox jumps over the lazy dog", "es");

		$this->assertEquals($linkifiedContent["new"], 0);
		$this->assertEquals($linkifiedContent["alternative"], 0);
		$this->assertEquals($linkifiedContent["unknown"], 0);
		$this->assertEquals($linkifiedContent["unknownTranslations"], []);
		$this->assertEquals($linkifiedContent["html"], "The quick brown fox jumps over the lazy dog");
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testSingleChapters() {
		$linkifiedContent = BibleLinker::linkify("John 3", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/JHN.3..NIV">John 3</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testOneChapterBooks() {
		$linkifiedContent = BibleLinker::linkify("3 John 10", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/3JN.1.10.NIV">3 John 10</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("2 John 1:3", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/2JN.1.3.NIV">2 John 1:3</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testChapterOutOfRange() {
		// don't link -- chapter doesn't exist
		$linkifiedContent = BibleLinker::linkify("Psalm 151", "en");
		$this->assertEquals("Psalm 151", $linkifiedContent["html"]);
		$this->assertNotEmpty($linkifiedContent["errors"]);
		$this->assertEquals($linkifiedContent["errors"]["Psalm 151"], BibleLinker::E_INVALID_CHAPTER);

		// link to first chapter (which is a valid chapter)
		$linkifiedContent = BibleLinker::linkify("Psalm 150-151", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/PSA.150..NIV">Psalm 150-151</a>',
			$linkifiedContent["html"],
		);
		// no error because we only link to first chapter in chapter range

		// link to first chapter (which is a valid chapter)
		$linkifiedContent = BibleLinker::linkify("Psalm 150; 151", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/PSA.150..NIV">Psalm 150</a>; 151',
			$linkifiedContent["html"],
		);
		$this->assertNotEmpty($linkifiedContent["errors"]);
		$this->assertEquals($linkifiedContent["errors"]["Psalm 150; 151"], BibleLinker::E_INVALID_CHAPTER);
	}

	function testVerseOutOfRange() {
		// don't link -- verse doesn't exist
		$linkifiedContent = BibleLinker::linkify("Psalm 150:7", "en");
		$this->assertEquals("Psalm 150:7", $linkifiedContent["html"]);
		$this->assertNotEmpty($linkifiedContent["errors"]);
		$this->assertEquals($linkifiedContent["errors"]["Psalm 150:7"], BibleLinker::E_INVALID_VERSE);

		// don't link -- verses don't exist
		$linkifiedContent = BibleLinker::linkify("Psalm 150:7-9", "en");
		$this->assertEquals("Psalm 150:7-9", $linkifiedContent["html"]);
		$this->assertNotEmpty($linkifiedContent["errors"]);
		$this->assertEquals($linkifiedContent["errors"]["Psalm 150:7-9"], BibleLinker::E_INVALID_VERSE);

		// link to first verse until end of chapter
		$linkifiedContent = BibleLinker::linkify("Psalm 150:5-7", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/PSA.150.5-6.NIV">Psalm 150:5-7</a>',
			$linkifiedContent["html"],
		);
		$this->assertNotEmpty($linkifiedContent["errors"]);
		$this->assertEquals($linkifiedContent["errors"]["Psalm 150:5-7"], BibleLinker::E_INVALID_VERSE);

		// link to first verse until end of chapter with single chapter book
		$linkifiedContent = BibleLinker::linkify("3 John 1-16", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/3JN.1.1-14.NIV">3 John 1-16</a>',
			$linkifiedContent["html"],
		);
		$this->assertNotEmpty($linkifiedContent["errors"]);
		$this->assertEquals($linkifiedContent["errors"]["3 John 1-16"], BibleLinker::E_INVALID_VERSE);
	}

	function testVerseRanges() {
		$linkifiedContent = BibleLinker::linkify("John 3:15-17", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/JHN.3.15-17.NIV">John 3:15-17</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testChapterRanges() {
		// link to first chapter only (Bible.com can't show multiple chapters at once)
		$linkifiedContent = BibleLinker::linkify("John 3-4", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/JHN.3..NIV">John 3-4</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testCompoundVerses() {
		$linkifiedContent = BibleLinker::linkify("John 3:16, 17", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/JHN.3.16.NIV">John 3:16</a>, <a href="https://www.bible.com/bible/111/JHN.3.17.NIV">17</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Genesis 1:1,2,3", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/GEN.1.1.NIV">Genesis 1:1</a>,<a href="https://www.bible.com/bible/111/GEN.1.2.NIV">2</a>,<a href="https://www.bible.com/bible/111/GEN.1.3.NIV">3</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testCompoundChapters() {
		$linkifiedContent = BibleLinker::linkify("John 3; 5", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/JHN.3..NIV">John 3</a>; <a href="https://www.bible.com/bible/111/JHN.5..NIV">5</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testCompoundVersesWithRanges() {
		$linkifiedContent = BibleLinker::linkify("Genesis 1:1,2-3", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/GEN.1.1.NIV">Genesis 1:1</a>,<a href="https://www.bible.com/bible/111/GEN.1.2-3.NIV">2-3</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testCompoundChaptersWithRanges() {
		$linkifiedContent = BibleLinker::linkify("Genesis 1:1; 2:3-5", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/GEN.1.1.NIV">Genesis 1:1</a>; <a href="https://www.bible.com/bible/111/GEN.2.3-5.NIV">2:3-5</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testSpecialDashesInRange() {
		// en dash
		$linkifiedContent = BibleLinker::linkify(htmlentities("Genesis 1:1–3"), "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/GEN.1.1-3.NIV">Genesis 1:1&ndash;3</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		// link to first chapter only (Bible.com can't show multiple chapters at once)
		$linkifiedContent = BibleLinker::linkify(htmlentities("Genesis 1–3"), "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/GEN.1..NIV">Genesis 1&ndash;3</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		// error with verse range
		$linkifiedContent = BibleLinker::linkify(htmlentities("Genesis 1:31–33"), "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/GEN.1.31-31.NIV">Genesis 1:31&ndash;33</a>',
			$linkifiedContent["html"],
		);
		$this->assertNotEmpty($linkifiedContent["errors"]);
		$this->assertEquals($linkifiedContent["errors"]["Genesis 1:31&ndash;33"], BibleLinker::E_INVALID_VERSE);

		// em dash
		$linkifiedContent = BibleLinker::linkify(htmlentities("Genesis 1:1—3"), "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/GEN.1.1-3.NIV">Genesis 1:1&mdash;3</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		// link to first chapter only (Bible.com can't show multiple chapters at once)
		$linkifiedContent = BibleLinker::linkify(htmlentities("Genesis 1—3"), "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/GEN.1..NIV">Genesis 1&mdash;3</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testSpecialCharsInBookName() {
		$linkifiedContent = BibleLinker::linkify(htmlentities("Oséias 2:3"), "pt");
		$this->assertEquals(
			'<a href="https://www.bible.com/pt/bible/129/HOS.2.3.NVI">' . htmlentities("Oséias 2:3") . "</a>",
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify(htmlentities("Lamentações 4:5"), "pt");
		$this->assertEquals(
			'<a href="https://www.bible.com/pt/bible/129/LAM.4.5.NVI">' . htmlentities("Lamentações 4:5") . "</a>",
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify(htmlentities("1º Samuel 6:7"), "pt");
		$this->assertEquals(
			'<a href="https://www.bible.com/pt/bible/129/1SA.6.7.NVI">' . htmlentities("1º Samuel 6:7") . "</a>",
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testImproperlyFormattedChapters() {
		$linkifiedContent = BibleLinker::linkify("Heb 11, 12:1", "en");
		$this->assertEquals("Heb 11, 12:1", $linkifiedContent["html"]);
		$this->assertNotEmpty($linkifiedContent["errors"]);
		$this->assertEquals($linkifiedContent["errors"]["Heb 11, 12:1"], BibleLinker::E_INVALID_SYNTAX);
	}

	function testDifferentTranslationsEnglish() {
		$linkifiedContent = BibleLinker::linkify("Psalm 119:105 (ESV)", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/59/PSA.119.105.ESV">Psalm 119:105</a> (ESV)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Psalm 119:105 (AMP)", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/1588/PSA.119.105.AMP">Psalm 119:105</a> (AMP)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Psalm 119:105 (KJV)", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/1/PSA.119.105.KJV">Psalm 119:105</a> (KJV)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testAlternateTranslationsEnglish() {
		$linkifiedContent = BibleLinker::linkify("Psalm 119:105 (NASB)", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/2692/PSA.119.105.NASB2020">Psalm 119:105</a> (NASB)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["alternative"]);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Psalm 119:105 (NIV2011)", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/PSA.119.105.NIV">Psalm 119:105</a> (NIV2011)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["alternative"]);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testTranslationNotFoundEnglish() {
		$linkifiedContent = BibleLinker::linkify("Psalm 119:105 (ABC)", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/PSA.119.105.NIV">Psalm 119:105</a> (ABC)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["unknown"]);
		$this->assertEquals("ABC", $linkifiedContent["unknownTranslations"][0]);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testDifferentTranslationsSpanish() {
		$linkifiedContent = BibleLinker::linkify("Salmos 119:105 (CON)", "es");
		$this->assertEquals(
			'<a href="https://www.bible.com/es/bible/567/PSA.119.105.CON">Salmos 119:105</a> (CON)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Salmos 119:105 (JBS)", "es");
		$this->assertEquals(
			'<a href="https://www.bible.com/es/bible/1076/PSA.119.105.JBS">Salmos 119:105</a> (JBS)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testAlternateTranslationsSpanish() {
		$linkifiedContent = BibleLinker::linkify("Salmos 119:105 (RVR)", "es");
		$this->assertEquals(
			'<a href="https://www.bible.com/es/bible/149/PSA.119.105.RVR1960">Salmos 119:105</a> (RVR)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["alternative"]);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testTranslationNotFoundSpanish() {
		$linkifiedContent = BibleLinker::linkify("Salmos 119:105 (RVR)", "es");
		$this->assertEquals(
			'<a href="https://www.bible.com/es/bible/149/PSA.119.105.RVR1960">Salmos 119:105</a> (RVR)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["alternative"]);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testDifferentTranslationsFrench() {
		$linkifiedContent = BibleLinker::linkify("Psaume 119:105 (BFC)", "fr");
		$this->assertEquals(
			'<a href="https://www.bible.com/fr/bible/63/PSA.119.105.BFC">Psaume 119:105</a> (BFC)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Psaume 119:105 (PDV2017)", "fr");
		$this->assertEquals(
			'<a href="https://www.bible.com/fr/bible/133/PSA.119.105.PDV2017">Psaume 119:105</a> (PDV2017)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testAlternateTranslationsFrench() {
		$linkifiedContent = BibleLinker::linkify("Psaume 119:105 (OSTERVALD)", "fr");
		$this->assertEquals(
			'<a href="https://www.bible.com/fr/bible/131/PSA.119.105.OST">Psaume 119:105</a> (OSTERVALD)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["alternative"]);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testTranslationNotFoundFrench() {
		$linkifiedContent = BibleLinker::linkify("Psaume 119:105 (ABC)", "fr");
		$this->assertEquals(
			'<a href="https://www.bible.com/fr/bible/93/PSA.119.105.LSG">Psaume 119:105</a> (ABC)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["unknown"]);
		$this->assertEquals("ABC", $linkifiedContent["unknownTranslations"][0]);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testDifferentTranslationsPortuguese() {
		$linkifiedContent = BibleLinker::linkify("Salmos 119:105 (A21)", "pt");
		$this->assertEquals(
			'<a href="https://www.bible.com/pt/bible/2645/PSA.119.105.A21">Salmos 119:105</a> (A21)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Salmos 119:105 (TB)", "pt");
		$this->assertEquals(
			'<a href="https://www.bible.com/pt/bible/277/PSA.119.105.TB">Salmos 119:105</a> (TB)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testTranslationsNotFoundPortuguese() {
		$linkifiedContent = BibleLinker::linkify("Salmos 119:105 (ABC)", "pt");
		$this->assertEquals(
			'<a href="https://www.bible.com/pt/bible/129/PSA.119.105.NVI">Salmos 119:105</a> (ABC)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["unknown"]);
		$this->assertEquals("ABC", $linkifiedContent["unknownTranslations"][0]);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testDifferentTranslationsGerman() {
		$linkifiedContent = BibleLinker::linkify("Psalm 119:105 (LUTHEUTE)", "de");
		$this->assertEquals(
			'<a href="https://www.bible.com/de/bible/3100/PSA.119.105.LUTHEUTE">Psalm 119:105</a> (LUTHEUTE)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Psalm 119:105 (HFA)", "de");
		$this->assertEquals(
			'<a href="https://www.bible.com/de/bible/73/PSA.119.105.HFA">Psalm 119:105</a> (HFA)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testAlternateTranslationsGerman() {
		$linkifiedContent = BibleLinker::linkify("Johannes 3:16 (NGU)", "de");
		$this->assertEquals(
			'<a href="https://www.bible.com/de/bible/108/JHN.3.16.NGU2011">Johannes 3:16</a> (NGU)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["alternative"]);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Psalm 119:105 (S2000)", "de");
		$this->assertEquals(
			'<a href="https://www.bible.com/de/bible/157/PSA.119.105.SCH2000">Psalm 119:105</a> (S2000)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["alternative"]);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testTranslationNotFoundGerman() {
		$linkifiedContent = BibleLinker::linkify("Psalm 119:105 (ABC)", "de");
		$this->assertEquals(
			'<a href="https://www.bible.com/de/bible/157/PSA.119.105.SCH2000">Psalm 119:105</a> (ABC)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["unknown"]);
		$this->assertEquals("ABC", $linkifiedContent["unknownTranslations"][0]);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testDifferentTranslationsRussian() {
		$linkifiedContent = BibleLinker::linkify("Иоанна 3:16 (SYNO)", "ru");
		$this->assertEquals(
			'<a href="https://www.bible.com/ru/bible/400/JHN.3.16.SYNO">Иоанна 3:16</a> (SYNO)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Иоанна 3:16 (RSP)", "ru");
		$this->assertEquals(
			'<a href="https://www.bible.com/ru/bible/201/JHN.3.16.RSP">Иоанна 3:16</a> (RSP)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Иоанна 3:16 (CARS)", "ru");
		$this->assertEquals(
			'<a href="https://www.bible.com/ru/bible/385/JHN.3.16.CARS">Иоанна 3:16</a> (CARS)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testTranslationNotFoundRussian() {
		$linkifiedContent = BibleLinker::linkify("Иоанна 3:16 (ABC)", "ru");
		$this->assertEquals(
			'<a href="https://www.bible.com/ru/bible/143/JHN.3.16.' . urlencode("НРП") . '">Иоанна 3:16</a> (ABC)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["unknown"]);
		$this->assertEquals("ABC", $linkifiedContent["unknownTranslations"][0]);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testDifferentTranslationsUkrainian() {
		$linkifiedContent = BibleLinker::linkify("Псалми 119:105 (UBIO)", "uk");
		$this->assertEquals(
			'<a href="https://www.bible.com/uk/bible/186/PSA.119.105.UBIO">Псалми 119:105</a> (UBIO)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Псалми 119:105 (УТТ)", "uk");
		$this->assertEquals(
			'<a href="https://www.bible.com/uk/bible/1755/PSA.119.105.' .
				urlencode("УТТ") .
				'">Псалми 119:105</a> (УТТ)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		$linkifiedContent = BibleLinker::linkify("Псалми 119:105 (НУП)", "uk");
		$this->assertEquals(
			'<a href="https://www.bible.com/uk/bible/3149/PSA.119.105.' .
				urlencode("НУП") .
				'">Псалми 119:105</a> (НУП)',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testTranslationNotFoundUkrainian() {
		$linkifiedContent = BibleLinker::linkify("Псалми 119:105 (ABC)", "uk");
		$this->assertEquals(
			'<a href="https://www.bible.com/uk/bible/204/PSA.119.105.UMT">Псалми 119:105</a> (ABC)',
			$linkifiedContent["html"],
		);
		$this->assertEquals(1, $linkifiedContent["unknown"]);
		$this->assertEquals("ABC", $linkifiedContent["unknownTranslations"][0]);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testWordBoundaries() {
		$linkifiedContent = BibleLinker::linkify("ge 1 Huge 1", "en");
		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/GEN.1..NIV">ge 1</a> Huge 1',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testDelinkify() {
		$enContent = '<a class="verse" href="https://www.bible.com/bible/111/PSA.119.105.NIV">Psalm 119:105</a>';
		$esContent =
			'<a href="https://www.bible.com/es/bible/149/PSA.119.105.RVR1960" class="verse">Salmos 119:105 (JBS)</a>';
		$frContent =
			'<a href="https://www.bible.com/fr/bible/93/PSA.119.105.LSG" target="_blank" >Psaumes 119:105 (LSG)</a>';
		$ptContent =
			'<a id="verse" href="https://www.bible.com/pt/bible/129/PSA.119.105.NVI" class="verse" >Salmos 119:105 (NTLH)</a>';

		// issue #8
		$problemContent =
			'<a class="verse" rel="noopener" href="https://www.bible.com/es/bible/89/DEU.28.15.LBLA" target="_blank">Deuteronomio 28:15</a> (LBLA) <em>Pero suceder&aacute; que si no obedeces al <sup>[<u><a href="https://www.biblegateway.com/passage/?search=deuteronomio+28%3A15&amp;version=LBLA#fes-LBLA-5627a" target="See footnote a">a</a></u>]</sup> Se&ntilde;or tu Dios, guardando todos sus mandamientos y estatutos que te ordeno hoy, <strong>VENDR&Aacute;N</strong> sobre ti todas estas maldiciones y te alcanzar&aacute;n:</em> (cf. <a class="verse" rel="noopener" href="https://www.bible.com/es/bible/149/EXO.19.5-6.RVR1960" target="_blank">Ex 19:5-6</a>; <a class="verse" rel="noopener" href="https://www.bible.com/es/bible/149/EXO.23.22.RVR1960" target="_blank">23:22</a>)';

		$this->assertEquals("Psalm 119:105", BibleLinker::delinkify($enContent));
		$this->assertEquals("Salmos 119:105 (JBS)", BibleLinker::delinkify($esContent));
		$this->assertEquals("Psaumes 119:105 (LSG)", BibleLinker::delinkify($frContent));
		$this->assertEquals("Salmos 119:105 (NTLH)", BibleLinker::delinkify($ptContent));
		$this->assertEquals(
			'Deuteronomio 28:15 (LBLA) <em>Pero suceder&aacute; que si no obedeces al <sup>[<u><a href="https://www.biblegateway.com/passage/?search=deuteronomio+28%3A15&amp;version=LBLA#fes-LBLA-5627a" target="See footnote a">a</a></u>]</sup> Se&ntilde;or tu Dios, guardando todos sus mandamientos y estatutos que te ordeno hoy, <strong>VENDR&Aacute;N</strong> sobre ti todas estas maldiciones y te alcanzar&aacute;n:</em> (cf. Ex 19:5-6; 23:22)',
			BibleLinker::delinkify($problemContent),
		);
	}

	function testAddingAttributesToVerse() {
		$linkifiedContent = BibleLinker::linkify("Psalm 119:105", "en", [
			"class" => "verse",
			"target" => "_blank",
			"rel" => "noopener",
			"onclick" => '$("div#showVerse").show()',
			"title" => "Click to open Bible.com",
		]);

		$this->assertEquals(
			'<a href="https://www.bible.com/bible/111/PSA.119.105.NIV" class="verse" target="_blank" rel="noopener" onclick="$(\"div#showVerse\").show()" title="Click to open Bible.com">Psalm 119:105</a>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testUrlEncodedBook() {
		$linkifiedContent = BibleLinker::linkify(htmlentities("Йоана 3:16 (НУП)"), "uk");
		$this->assertEquals(
			'<a href="https://www.bible.com/uk/bible/3149/JHN.3.16.' .
				urlencode("НУП") .
				'">' .
				htmlentities("Йоана 3:16") .
				"</a> " .
				htmlentities("(НУП)"),
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	/**
	 * Issue #7: https://gitrenewal.com/church-renewal-public/bible-linker/-/issues/7
	 */
	function testCommaSeparatedVerses() {
		$content =
			"<p>Review John 15:5-6, Romans 12:2, Matthew 28:18-20, 1 Peter 5:8, John 10:27, James 1:22, Psalm 1, Colossians 3:2, Psalm 39:4, Matthew 5:16, Matthew 6:9-13.</p>";
		$linkifiedContent = BibleLinker::linkify($content, "en");
		$this->assertEquals(
			'<p>Review <a href="https://www.bible.com/bible/111/JHN.15.5-6.NIV">John 15:5-6</a>, <a href="https://www.bible.com/bible/111/ROM.12.2.NIV">Romans 12:2</a>, <a href="https://www.bible.com/bible/111/MAT.28.18-20.NIV">Matthew 28:18-20</a>, <a href="https://www.bible.com/bible/111/1PE.5.8.NIV">1 Peter 5:8</a>, <a href="https://www.bible.com/bible/111/JHN.10.27.NIV">John 10:27</a>, <a href="https://www.bible.com/bible/111/JAS.1.22.NIV">James 1:22</a>, <a href="https://www.bible.com/bible/111/PSA.1..NIV">Psalm 1</a>, <a href="https://www.bible.com/bible/111/COL.3.2.NIV">Colossians 3:2</a>, <a href="https://www.bible.com/bible/111/PSA.39.4.NIV">Psalm 39:4</a>, <a href="https://www.bible.com/bible/111/MAT.5.16.NIV">Matthew 5:16</a>, <a href="https://www.bible.com/bible/111/MAT.6.9-13.NIV">Matthew 6:9-13</a>.</p>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testInlineSpanning() {
		// This should link to John 15:5
		$content = '<p>Review <span>John</span> 15:<span style="color:blue">5</span>-6</p>';
		$linkifiedContent = BibleLinker::linkify($content, "en");
		$this->assertEquals(
			'<p>Review <span><a href="https://www.bible.com/bible/111/JHN.15.5-6.NIV">John 15:5-6</a></span><span style="color:blue"></span></p>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

	function testBlockSpanning() {
		// This should NOT link to John 15:5, but instead to just John 15.
		$content = "<p>Review <span>John<span> 15</p><p>:5</p>";
		$linkifiedContent = BibleLinker::linkify($content, "en");
		$this->assertEquals(
			'<p>Review <span><a href="https://www.bible.com/bible/111/JHN.15..NIV">John 15</a><span></p><p>:5</p>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);

		// This should NOT link to John 15:5, but instead to just John 15.
		$content = '<p>Review <span>John<span> 15</p><p style="font-weight:bold">:5</p>';
		$linkifiedContent = BibleLinker::linkify($content, "en");
		$this->assertEquals(
			'<p>Review <span><a href="https://www.bible.com/bible/111/JHN.15..NIV">John 15</a><span></p><p style="font-weight:bold">:5</p>',
			$linkifiedContent["html"],
		);
		$this->assertEmpty($linkifiedContent["errors"]);
	}

    function testEmEnDashesInReference() {
		$emDashVerse = "<p>This verse Exodus 19:4–6 sometimes doesn't work for some reason.<p>";
        $otherEmDashVerse = "<p>Study Gen 1:1–3 and answer the following questions:<p>";
        $enDashVerse = "<p>Please memorize Rom 12:1—4 by next week.</p>";
		
        $linkifiedEmDashContent = BibleLinker::linkify($emDashVerse, "en");
        $this->assertEquals('<p>This verse <a href="https://www.bible.com/bible/111/EXO.19.4-6.NIV">Exodus 19:4–6</a> sometimes doesn\'t work for some reason.<p>', $linkifiedEmDashContent["html"]);
		
        $linkifiedEmDashContent = BibleLinker::linkify($otherEmDashVerse, "en");
        $this->assertEquals('<p>Study <a href="https://www.bible.com/bible/111/GEN.1.1-3.NIV">Gen 1:1–3</a> and answer the following questions:<p>', $linkifiedEmDashContent["html"]);

        $linkifiedEnDashContent = BibleLinker::linkify($enDashVerse, "en");
        $this->assertEquals('<p>Please memorize <a href="https://www.bible.com/bible/111/ROM.12.1-4.NIV">Rom 12:1—4</a> by next week.</p>', $linkifiedEnDashContent["html"]);
    }
}
