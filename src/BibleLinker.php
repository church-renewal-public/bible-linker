<?php

namespace ChurchRenewal\BibleLinker;

class BibleLinker {
    const E_INVALID_SYNTAX = -1;
    const E_INVALID_BOOK = -2;
    const E_INVALID_CHAPTER = -3;
    const E_INVALID_VERSE = -4;

    public static function isLanguageSupported(string $languageId): bool {
        return Abbreviations::isLanguageSupported($languageId) && Translations::isLanguageSupported($languageId);
    }

    public static function delinkify(string $linkifiedContent): string {
        return preg_replace('/(<a.[^>]*href=[\'|"]https:\/\/www\.bible\.com\/?\w?\w?\/bible\/\d+\/.+[\'|"].*>(.+)<\/a>)/mU', '$2', $linkifiedContent);
    }

    /**
     * Returns null if language is not supported, or an associative array containing keys:
     *  'new'                   int - number of new links created,
     *  'alternative'           int number of alternative translations used,
     *  'unknown'               int - number of unknown translations,
     *  'unknownTranslations'   array - all translations not found in Translations,
     *  'html'                  string - the new linkified html
     *  'errors'                array - an associative array of parsing errors; each key is the string that failed, each value is the error code
     */
    public static function linkify(string $content, string $languageId, array $attributes = []): array {
        $results = [
            'new' => 0,
            'alternative' => 0,
            'unknown' => 0,
            'unknownTranslations' => [],
            'html' => $content,
            'errors' => []
        ];

        if (!Self::isLanguageSupported($languageId)) return $results;

        $content = Self::delinkify($content);

        $tagger = new Tagger($content);
        $plain = $tagger->getPlainText();

        preg_match_all(Self::bookNameRegex($languageId), $plain, $bookMatches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);

        if (count($bookMatches) == 0) return $results;

        $bookRanges = [];
        $bookRanges[] = [0, $bookMatches[0][0][1]];
        for ($i = 0; $i < count($bookMatches) - 1; $i++) {
            $bookRanges[] = [$bookMatches[$i][0][1], $bookMatches[$i + 1][0][1]];
        }
        $bookRanges[] = [$bookMatches[count($bookMatches) - 1][0][1], strlen($plain) + 1];

        foreach ($bookRanges as $range) {
            $rangePlain = substr($plain, $range[0], $range[1] - $range[0]);
            preg_match_all(Self::regex($languageId), $rangePlain, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);

            foreach ($matches as $match) {
                $linkifiedMatch = Self::linkifyReferences($tagger, $match, $languageId, $range[0], $attributes);
                $results['new'] += $linkifiedMatch['new'];
                $results['unknown'] += $linkifiedMatch['unknown'];
                $results['alternative'] += $linkifiedMatch['alternative'];
                $results['unknownTranslations'] = array_merge($results['unknownTranslations'], $linkifiedMatch['unknownTranslations']);
                $results['errors'] = array_merge($results['errors'], $linkifiedMatch['errors']);
            }
        }

        $results['html'] = $tagger->getHTML();
        return $results;
    }

    private static function regex(string $languageId): string {
        // This should match: 'John 10:12; 14:16,18; 19 (NIV)'
        $books = '(' . implode('|', Self::allBookNames($languageId)) . ')';

        if ($languageId == 'fr') {
            // French references separate verses with a period, instead of a colon
            return '/(?:^|[^\w])' . $books . '\s*(\d(?:\d|\s|\-|\:|;|,|\.|&ndash;|&mdash;|–|—)*)\s*(\(\s*\w*\s*\))?' . '/miu';
        } else {
            return '/(?:^|[^\w])' . $books . '\s*(\d(?:\d|\s|\-|\:|;|,|&ndash;|&mdash;|–|—)*)\s*(\(\s*\w*\s*\))?' . '/miu';
        }
    }

    public static function bookNameRegex(string $languageId): string {
        // This should match: 'John 10:12; 14:16,18; 19 (NIV)'
        $books = implode('|', Self::allBookNames($languageId));

        // so it matches an abbreviated book name with or without a period (e.g. Mat 6:1 or Mat. 6:1)
        $books .= '|' . implode('\.|', Self::allBookNames($languageId)) . '\.';

        return '/(?:^|[^\w])(?:' . $books . ')\b/miu';
    }

    private static function allBookNames(string $languageId): array {
        $allBooks = [];

        foreach (Abbreviations::getLanguage($languageId) as $abbrev => $abbreviations) {
            $allBooks[] = $abbrev;
            foreach ($abbreviations as $ab) {
                $allBooks[] = $ab;

                $escaped = htmlspecialchars($ab);
                if ($escaped != $ab) {
                    $allBooks[] = $escaped;
                }

                $escaped2 = htmlentities($ab);
                if ($escaped2 != $ab && $escaped2 != $escaped) {
                    $allBooks[] = $escaped2;
                }
            }
        }

        usort($allBooks, function ($a, $b) {
            return strlen($b) - strlen($a);
        });

        return $allBooks;
    }

    /**
     * This returns the total string length of all the strings in the array up to, but not including, this index.
     */
    private static function stringPartLength(array $stringArray, int $index, int $extra = 1): int {
        $count = 0;

        for ($i = 0; $i < $index; $i++) {
            $count += strlen($stringArray[$i]) + $extra;
        }

        return $count;
    }

    private static function linkifyReferences(Tagger $tagger, array $matches, string $languageId, int $positionOffset, array $attributes = []): array {
        $result = [
            'new' => 0,
            'alternative' => 0,
            'unknown' => 0,
            'unknownTranslations' => [],
            'errors' => [],
        ];

        $book = html_entity_decode($matches[1][0]);
        $bookPos = $matches[1][1] + $positionOffset;
        $referenceStuff = $matches[2][0];
        $referenceStuffPos = $matches[2][1] + $positionOffset;
        $translation = $matches[3][0] ?? null;

        // replace html codes with something that won't get caught by our verse parser, but has the same length so link insertion still works
        $referenceStuff = str_replace(['&ndash;', '&mdash;'], ['[ndash]', '[mdash]'], $referenceStuff);
        $chapReferences = array_merge(explode(';', $referenceStuff));

        // Just a book, e.g. 'John'.  We don't want to put a link in a sentence like 'John Smith is super cool!'.
        if (count($chapReferences) == 0) return $result;

        $bookAbbreviation = null;
        foreach (Abbreviations::getLanguage($languageId) as $abbrev => $abbreviations) {
            if (in_array(strtoupper($book), array_map(fn($abv) => strtoupper($abv), $abbreviations)) || $abbrev == $book) {
                $bookAbbreviation = $abbrev;
            }
        }

        // book abbreviation not found
        if (is_null($bookAbbreviation)) {
            $result['errors'][$matches[0][0]] = Self::E_INVALID_BOOK;
            return $result;
        }

        $linker = new RangeLinkifier($tagger, $languageId, $translation);

        if (!$linker->translationFound()) {
            if ($linker->alternativeFound()) {
                $result['alternative']++;
            } else {
                $result['unknown']++;
                $result['unknownTranslations'][] = $linker->getOriginalTranslation();
            }
        }

        for ($chapIndex = 0; $chapIndex < count($chapReferences); $chapIndex++) {
            $chapReference = $chapReferences[$chapIndex]; // $chapReference might be '10:12' or '10:12-14' or '10:12,14' or '10:12-14,18'

            // In case the reference is 'John 1:2-3; something else', there will be a blank verse part after the semicolon
            if (trim($chapReference) == '') continue;

            if ($languageId == 'fr' && strpos($chapReference, ':') === false) {
                $chapParts = explode('.', $chapReference);  // index zero is the chapter number, and index one is all verse stuff
            } else {
                $chapParts = explode(':', $chapReference);  // index zero is the chapter number, and index one is all verse stuff
            }
            $chapter = $chapParts[0];   // e.g. '3'

            if (count($chapParts) == 1) {
                // No colon, so it's just a chapter with no verse stuff e.g. '13' OR if the book only has one chapter, then it's a verse

                $verse = null;
                if (in_array($bookAbbreviation, Abbreviations::oneChapterBooks())) {
                    // this reference doesn't contain a chapter reference because it's implicitly a 1, like 2 John 10
                    $verse = $chapter;
                    $firstChapter = 1;

                    // make sure that the verses referenced are verses that exist
                    $verses = $verse;
                    $verseCount = Abbreviations::getVerseCount($bookAbbreviation, $firstChapter);
                    if ($verseCount < 0) {
                        $result['errors'][$matches[0][0]] = $verseCount;
                        continue;
                    }

                    $verseArray = explode('-', str_replace(['[ndash]', '[mdash]'], '-', $verse));
                    if (count($verseArray) > 1) {
                        if ($verseArray[0] > $verseCount) {
                            // if this is true - first verse in a range doesn't exist, so just skip it
                            $result['errors'][$matches[0][0]] = Self::E_INVALID_VERSE;
                            continue;
                        }
                        if ($verseArray[1] > $verseCount) {
                            // if this is true - the first verse is valid, but the second one isn't, so make a link to the end of the chapter
                            $result['errors'][$matches[0][0]] = Self::E_INVALID_VERSE;
                            $verse = $verseArray[0] . '-' . $verseCount;
                        }
                    } else {
                        if ($verseArray[0] > $verseCount) {
                            // if this is true - the verse doesn't exist, so skip it
                            $result['errors'][$matches[0][0]] = Self::E_INVALID_VERSE;
                            continue;
                        }
                    }

                } else {
                    // if it's a range, only link to the first chapter because Bible.com can only show 1 chapter per page e.g. 'John 12-14'
                    $firstChapter = (int) explode('-', str_replace(['[ndash]', '[mdash]'], '-', $chapter))[0];
                }

                // this chapter doesn't actually exist, so skip it
                $chapterCount = Abbreviations::getChapterCount($bookAbbreviation);
                if ($chapterCount < 0) {
                    // < 0 means $chapterCount is an error code
                    $result['errors'][$matches[0][0]] = $chapterCount;
                    continue;
                }
                if ($firstChapter > $chapterCount) {
                    $result['errors'][$matches[0][0]] = Self::E_INVALID_CHAPTER;
                    continue;
                }

                if ($chapIndex == 0) {
                    // We are the first chapter, right after the book. e.g. 'John 13'
                    $startAt = $bookPos;
                    $length = $referenceStuffPos + strlen($chapter) - $bookPos;
                    $linker->link($startAt, $length, $bookAbbreviation, $firstChapter, $verse, $attributes);
                } else {
                    // We are a simple chapter (no verse), not right after the book, like '13' in 'John 12; 13'
                    $startAt = $referenceStuffPos + Self::stringPartLength($chapReferences, $chapIndex);
                    $length = strlen($chapReference);
                    $linker->link($startAt, $length, $bookAbbreviation, $firstChapter, $verse, $attributes);
                }
            } else {
                // There was verse information

                $allVerseParts = $chapParts[1]; // e.g. '12-14,18'
                $verseParts = explode(',', $allVerseParts); // e.g. ['12-14', '18']

                if (count($verseParts) == 0 || (count($verseParts) == 1 && trim($verseParts[0]) == '')) {
                    // There are no verse parts.  This could be something like this:
                    // Essayez à nouveau ÉMORP en utilisant le célèbre Psaume 23.
                    // So link to the chapter as a chapter, the same as we do a few lines up
                    // OR if the book only has one chapter, then it's a verse

                    $verse = null;
                    if (in_array($bookAbbreviation, Abbreviations::oneChapterBooks())) {
                        // this reference doesn't contain a chapter reference because it's implicitly a 1, like 2 John 10
                        $verse = $chapter;
                        $chapter = 1;
                    }

                    // this chapter doesn't actually exist, so skip it
                    $chapterCount = Abbreviations::getChapterCount($bookAbbreviation);
                    if ($chapterCount < 0) {
                        // < 0 means $chapterCount is an error code
                        $result['errors'][$matches[0][0]] = $chapterCount;
                        continue;
                    }
                    if ($chapter > $chapterCount) {
                        $result['errors'][$matches[0][0]] = Self::E_INVALID_CHAPTER;
                        continue;
                    }

                    if ($chapIndex == 0) {
                        // We are the first chapter, right after the book. e.g. 'John 13'
                        $startAt = $bookPos;
                        $length = $referenceStuffPos + strlen($chapter) - $bookPos;
                        $linker->link($startAt, $length, $bookAbbreviation, $chapter, $verse, $attributes);
                    } else {
                        // We are a simple chapter (no verse), not right after the book, like '13' in 'John 12; 13'
                        $startAt = $referenceStuffPos + Self::stringPartLength($chapReferences, $chapIndex);
                        $length = strlen($chapReference);
                        $linker->link($startAt, $length, $bookAbbreviation, $chapter, $verse, $attributes);
                    }
                }

                for ($versePartsIndex = 0; $versePartsIndex < count($verseParts); $versePartsIndex++) {
                    $versePart = $verseParts[$versePartsIndex]; // e.g. '12-14' or '18' (but nothing with a comma)

                    // In case the reference is 'John 1:2-3, something else', there will be a blank verse part after the comma
                    if (trim($versePart) == '') continue;

                    // make sure that the verses referenced are verses that exist
                    $verses = $versePart;
                    $verseCount = Abbreviations::getVerseCount($bookAbbreviation, $chapter);
                    if ($verseCount < 0) {
                        $result['errors'][$matches[0][0]] = $verseCount;
                        continue;
                    }

                    $verseArray = explode('-', str_replace(['[ndash]', '[mdash]', '–', '—'], '-', $versePart));
                    if (count($verseArray) > 1) {
                        if ($verseArray[0] > $verseCount) {
                            // if this is true - first verse in a range doesn't exist, so just skip it
                            $result['errors'][$matches[0][0]] = Self::E_INVALID_VERSE;
                            continue;
                        }
                        if ($verseArray[1] > $verseCount) {
                            // if this is true - the first verse is valid, but the second one isn't, so make a link to the end of the chapter
                            $result['errors'][$matches[0][0]] = Self::E_INVALID_VERSE;
                            $verses = $verseArray[0] . '-' . $verseCount;
                        }
                    } else {
                        if ($verseArray[0] > $verseCount) {
                            // if this is true - the verse doesn't exist, so skip it
                            $result['errors'][$matches[0][0]] = Self::E_INVALID_VERSE;
                            continue;
                        }
                    }

                    if ($versePartsIndex == 0) {
                        // This verse is right next to the chapter, like '14' in 'John 12:14' or 'John 3; 10:14'
                        if ($chapIndex == 0) {
                            // We are the first chapter, right after the book. e.g. 'John 13:14'
                            $startAt = $bookPos;
                            $length = $referenceStuffPos + strlen($chapter) + 1 + strlen($versePart) - $bookPos;
                            $linker->link($startAt, $length, $bookAbbreviation, $chapter, $verses, $attributes);
                        } else {
                            // We are the NOT the first chapter right after the book, but we ARE the first verse after the chapter, like the '14' in 'John 3; 10:14'
                            $startAt = $referenceStuffPos + Self::stringPartLength($chapReferences, $chapIndex);
                            $length = strlen($chapter) + 1 + strlen($versePart);
                            $linker->link($startAt, $length, $bookAbbreviation, $chapter, $verses, $attributes);
                        }
                    } else {
                        // This verse is NOT right next to the chapter, like '8' in 'a John 1; 2; 3:4; 5:6-7,8 a'
                        $startAt =
                            $referenceStuffPos +    // the beginning of the reference stuff
                            Self::stringPartLength($chapReferences, $chapIndex) + // all the chapters before this one e.g. '1; 2; 3:4'
                            strlen($chapter) + // our chapter e.g. '5'
                            1 + // the colon after the chapter e.g. ':'
                            Self::stringPartLength($verseParts, $versePartsIndex) // all the verses before this one e.g. '6-7'
                            // the comma is included in the stringPartLength
                        ;
                        $length = strlen($versePart);   // e.g. '8'
                        $linker->link($startAt, $length, $bookAbbreviation, $chapter, $verses, $attributes);
                    }
                }
            }
        }

        $result['new'] = $linker->getNew();
        return $result;
    }
}
