<?php

namespace ChurchRenewal\BibleLinker;

class RangeLinkifier {
    private Tagger $tagger;
    private int $new = 0;
    private string $languageId;
    private string $translation;
    private string $translationId;
    private string $originalTranslation;
    private bool $translationFound = false;
    private bool $alternativeFound = false;

    public function __construct(Tagger $tagger, string $languageId, ?string $translation = null) {
        $this->tagger = $tagger;
        $this->languageId = $languageId;

        if ($translation == null) {
            $translation = Translations::getDefaultTranslationId($languageId);
        }

        $translation = strtoupper(preg_replace('/[\(\)\s]/', '', $translation));
        $this->originalTranslation = $translation;

        $transId = Translations::getTranslationId($this->languageId, $translation);
        if ($transId == null) {
            $this->translationFound = false;
            $translation = Translations::alternativeTranslation($translation);

            if ($translation == null || Translations::getTranslationId($this->languageId, $translation) == null) {
                // The original translation was not found, and there is no alternative, so use the default
                $this->alternativeFound = false;
                $this->translation = Translations::getDefaultTranslationId($languageId);
            } else {
                // The original translation was not found, but we found an alternative
                $this->alternativeFound = true;
                $this->translation = $translation;
            }

            $this->translationId = Translations::getTranslationId($this->languageId, $this->translation);
        } else {
            // The original (or default if not specified) translation was found
            $this->translationFound = true;
            $this->translation = $translation;
            $this->translationId = $transId;
        }
    }

    public function getNew(): int {
        return $this->new;
    }

    public function translationFound(): bool {
        return $this->translationFound;
    }

    public function alternativeFound(): bool {
        return $this->alternativeFound;
    }

    public function getOriginalTranslation(): string {
        return $this->originalTranslation;
    }

    /**
     * Makes the text into a link
     */
    public function link(int $start, int $length, string $bookAbbreviation, string $chapter, null|string|int $verse = null, array $attributes = []): void {
        while ($this->tagger->getPlainText()[$start] == ' ' || $this->tagger->getPlainText()[$start] == ',') {
            $start++;
            $length--;
        }

        while ($this->tagger->getPlainText()[$start + $length - 1] == ' ' || $this->tagger->getPlainText()[$start + $length - 1] == ',') {
            $length--;
        }

        $replaceTag = false;

        if ($this->tagger->isInATag($start, $length)) {
            $replaceTag = true;
        }

        // if we previously replaced an en or em dash, replace it with a hyphen for the url
        // or replace em and en dashes with hyphens
        if ($verse) {
			$verse = str_replace(['[ndash]', '[mdash]', '–', '—'], '-', $verse);
		}

        /**
         * e.g.
         *   https://www.bible.com/bible/111/JON.3.7-10.NIV
         *   https://www.bible.com/es/bible/111/JON.3.7-10.NIV
         */
        $bibleLang = $this->languageId != 'en' ? "/$this->languageId" : '';
        $translation = urlencode($this->translation);
        $url = "https://www.bible.com$bibleLang/bible/$this->translationId/$bookAbbreviation.$chapter.$verse.$translation";

        $url = preg_replace('/\s+/', '', $url);

        $this->tagger->detagify($start, $length);
        if ($replaceTag) {
            $this->tagger->removeATag($start, $length);
        }

        $linkAttr = '';
        foreach ($attributes as $attr => $value) {
            $linkAttr .= ' ' . $attr . '="' . addslashes($value) . '"';
        }

        $this->tagger->surroundWithTags($start, $length, "<a href=\"{$url}\"{$linkAttr}>", "</a>");

        $this->new++;
    }
}
