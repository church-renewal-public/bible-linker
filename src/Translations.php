<?php

namespace ChurchRenewal\BibleLinker;

class Translations {
    private static array $translations = [
        // Deutsch (German)
        'de' => [
            'BIBEL.HEUTE' => [877, 'bibel heute'],
            'DELUT' => [51, 'Lutherbibel 1912'],
            'ELB' => [57, 'Darby Unrevidierte Elberfelder'],
            'ELB71' => [58, 'Elberfelder 1871'],
            'ELBBK' => [2351, 'Elberfelder Übersetzung (Version von bibelkommentare.de)'],
            'GANTP' => [65, 'Albrecht NT und Psalmen'],
            'HFA' => [73, 'Hoffnung für alle'],
            'LUTHEUTE' => [3100, 'luther.heute'],
            'NGU2011' => [108, 'Neue Genfer Übersetzung'],
            'SCH1951' => [158, 'Die Heilige Schrift (Schlachter 1951)'],
            'SCH2000' => [157, 'Die Bibel (Schlachter 2000)'],
            'TKW' => [2200, 'Textbibel von Kautzsch und Weizsäcker'],
        ],

        // English
        'en' => [
            'AMP' => [1588, 'AMP Amplified Bible'],
            'AMPC' => [8, 'AMPC Amplified Bible Classic Edition'],
            'ASV' => [12, 'ASV American Standard Version'],
            'BOOKS' => [31, 'BOOKS The Books of the Bible NT'],
            'BSB' => [3034, 'BSB English: Berean Standard Bible'],
            'CEB' => [37, 'CEB Common English Bible'],
            'CEV' => [392, 'CEV Contemporary English Version'],
            'CEVDCI' => [303, 'CEVDCI Contemporary English Version Interconfessional Edition'],
            'CEVUK' => [294, 'CEVUK Contemporary English Version (Anglicised) 2012'],
            'CJB' => [1275, 'CJB Complete Jewish Bible'],
            'CPDV' => [42, 'CPDV Catholic Public Domain Version'],
            'CSB' => [1713, 'CSB Christian Standard Bible'],
            'DARBY' => [478, 'DARBY Darby\'s Translation 1890'],
            'DRC1752' => [55, 'DRC1752 Douay-Rheims Challoner Revision 1752'],
            'EASY' => [2079, 'EASY EasyEnglish Bible 2018'],
            'ERV' => [406, 'ERV Holy Bible: Easy-to-Read Version'],
            'ESV' => [59, 'ESV English Standard Version 2016'],
            'FBV' => [1932, 'FBV Free Bible Version'],
            'GNBDC' => [416, 'GNBDC Good News Bible (Anglicised)'],
            'GNBDK' => [431, 'GNBDK Good News Bible (Catholic edition in Septuagint order)'],
            'GNBUK' => [296, 'GNBUK Good News Bible (Anglicised) 1994'],
            'GNT' => [68, 'GNT Good News Translation'],
            'GNTD' => [69, 'GNTD Good News Translation (US Version)'],
            'GNV' => [2163, 'GNV Geneva Bible'],
            'GW' => [70, 'GW GOD\'S WORD'],
            'GWC' => [1047, 'GWC St Paul from the Trenches 1916'],
            'HCSB' => [72, 'HCSB Holman Christian Standard Bible'],
            'ICB' => [1359, 'ICB International Children’s Bible'],
            'JUB' => [1077, 'JUB Jubilee Bible'],
            'KJV' => [1, 'KJV King James Version'],
            'KJVAAE' => [546, 'KJVAAE King James Version with Apocrypha American Edition'],
            'KJVAE' => [547, 'KJVAE King James Version American Edition'],
            'LEB' => [90, 'LEB Lexham English Bible'],
            'MEV' => [1171, 'MEV Modern English Version'],
            'MP1650' => [1365, 'MP1650 Psalms of David in Metre 1650 (Scottish Psalter)'],
            'MP1781' => [3051, 'MP1781 Scottish Metrical Paraphrases 1781'],
            'MSG' => [97, 'MSG The Message'],
            'NABRE' => [463, 'NABRE New American Bible revised edition'],
            'NASB1995' => [100, 'NASB1995 New American Standard Bible - NASB 1995'],
            'NASB2020' => [2692, 'NASB2020 New American Standard Bible - NASB'],
            'NCV' => [105, 'NCV New Century Version'],
            'NET' => [107, 'NET New English Translation'],
            'NIRV' => [110, 'NIRV New International Reader’s Version'],
            'NIV' => [111, 'NIV New International Version'],
            'NIVUK' => [113, 'NIVUK New International Version (Anglicised)'],
            'NKJV' => [114, 'NKJV New King James Version'],
            'NLT' => [116, 'NLT New Living Translation'],
            'NMV' => [2135, 'NMV New Messianic Version Bible'],
            'NRSV' => [2016, 'NRSV New Revised Standard Version'],
            'NRSVCI' => [2015, 'NRSV-CI New Revised Standard Version Catholic Interconfessional'],
            'PEV' => [2530, 'PEV Plain English Version'],
            'RAD' => [2753, 'RAD Radiate New Testament'],
            'RSV' => [2020, 'RSV Revised Standard Version'],
            'RSVCI' => [2017, 'RSV-CI Revised Standard Version'],
            'RV1885' => [477, 'RV1885 Revised Version 1885'],
            'RV1895' => [1922, 'RV1895 Revised Version with Apocrypha 1885 1895'],
            'TEG' => [3010, 'TEG Isaiah 1830 1842 (John Jones alias Ioan Tegid)'],
            'TLV' => [314, 'TLV Tree of Life Version'],
            'TOJB2011' => [130, 'TOJB2011 The Orthodox Jewish Bible'],
            'TPT' => [1849, 'TPT The Passion Translation'],
            'TS2009' => [316, 'TS2009 The Scriptures 2009'],
            'WBMS' => [2407, 'WBMS Wycliffe\'s Bible with Modern Spelling'],
            'WEB' => [206, 'WEB World English Bible'],
            'WEBBE' => [1204, 'WEBBE World English Bible British Edition'],
            'WMB' => [1209, 'WMB World Messianic Bible'],
            'WMBBE' => [1207, 'WMBBE World Messianic Bible British Edition'],
            'YLT98' => [821, 'YLT98 Young\'s Literal Translation 1898'],
        ],

        // Español (Spanish)
        'es' => [
            'BDO1573' => [1715, 'BDO1573 Biblia del Oso 1573'],
            'BHTI' => [222, 'BHTI La Biblia Hispanoamericana (Traducción Interconfesional,  versión hispanoamericana)'],
            'BLPH' => [28, 'BLPH La Palabra (versión hispanoamericana)'],
            'CON' => [567, 'CON Cofán: Chiga Tevaen\'jen'],
            'DHH94I' => [52, 'DHH94I Biblia Dios Habla Hoy'],
            'DHH94PC' => [411, 'DHH94PC Biblia Dios Habla Hoy'],
            'DHHDK' => [1845, 'DHHDK Dios Habla Hoy DK'],
            'DHHS94' => [1846, 'DHHS94 Dios habla Hoy Estándar 1994'],
            'JBS' => [1076, 'JBS Biblia del Jubileo'],
            'LBLA' => [89, 'LBLA La Biblia de las Américas'],
            'NBLA' => [103, 'NBLA Nueva Biblia de las Américas'],
            'NBV' => [753, 'NBV Nueva Biblia Viva'],
            'NTV' => [127, 'NTV Nueva Traducción Viviente'],
            'NVI' => [128, 'NVI Nueva Versión Internacional - Español'],
            'NVIS' => [2664, 'NVIS La Santa Biblia,  Nueva Versión Internacional Simplificada '],
            'PDT' => [197, 'PDT La Biblia: La Palabra de Dios para todos'],
            'RVA2015' => [1782, 'RVA2015 Reina Valera Actualizada'],
            'RVC' => [146, 'RVC Reina Valera Contemporánea'],
            'RVES' => [147, 'RVES Reina-Valera Antigua'],
            'RVR09' => [1718, 'RVR09 Biblia Reina Valera 1909'],
            'RVR1960' => [149, 'RVR1960 Biblia Reina Valera 1960'],
            'RVR95' => [150, 'RVR95 Biblia Reina Valera 1995'],
            'TLA' => [176, 'TLA Traducción en Lenguaje Actual'],
            'TLAI' => [178, 'TLAI Traducción en Lenguaje Actual Interconfesional'],
            'VBLNT' => [1952, 'VBL El Nuevo Testamento,  Versión Biblia Libre'],
            'VBL' => [3291, 'VBL Versión Biblia Libre'],
        ],

        // Français (French)
        'fr' => [
            'BCC1923' => [504, 'BCC1923 Bible catholique Crampon 1923'],
            'BDS' => [21, 'BDS La Bible du Semeur 2015'],
            'BEX2004' => [3286, 'BEX2004 La Bible expiquée'],
            'BFC' => [63, 'BFC Bible en français courant'],
            'FMAR' => [62, 'FMAR Martin 1744'],
            'FRDBY' => [64, 'FRDBY Bible Darby en français'],
            'LSG' => [93, 'LSG La Sainte Bible par Louis Segond 1910'],
            'NBS' => [104, 'NBS Nouvelle Bible Segond'],
            'NEG79' => [106, 'NEG79 Nouvelle Edition de Genève 1979'],
            'NFC' => [2367, 'NFC Nouvelle Français courant'],
            'NVS78P' => [2053, 'NVS78P Nouvelle Segond révisée'],
            'OST' => [131, 'OST Ostervald'],
            'PDV2017' => [133, 'PDV2017 Parole de Vie 2017'],
            'S21' => [152, 'S21 Bible Segond 21'],
            'SACY' => [2599, 'SACY Évangile de Saint Matthieu 1855 (de Sacy)'],
        ],

        // Português (Portuguese)
        'pt' => [
            'A21' => [2645, 'A21 Biblia Almeida Século 21'],
            'ARA' => [1608, 'ARA Almeida Revista e Atualizada'],
            'ARC' => [212, 'ARC Almeida Revista e Corrigida'],
            'BLT' => [3254, 'BLT Biblia Livre Para Todos'],
            'NAA' => [1840, 'NAA Nova Almeida Atualizada'],
            'NBV' => [1966, 'NBV-P Nova Bíblia Viva Português'],
            'NTLH' => [211, 'NTLH Nova Tradução na Linguagem de Hoje'],
            'NVI' => [129, 'NVI Nova Versão Internacional - Português'],
            'NVT' => [1930, 'NVT Bíblia Sagrada,  Nova Versão Transformadora'],
            'TB' => [277, 'TB Tradução Brasileira'],
            'VFL' => [200, 'VFL Bíblia Sagrada: Versão Fácil de Ler'],
        ],

        // Русский (Russian)
        'ru' => [
            'BTI' => [313, 'Библия под ред. М.П. Кулакова и М.М. Кулакова'],
            'CARS' => [385, 'Восточный Перевод'],
            'CARS-A' => [840, 'Восточный перевод, версия с «Аллахом»'],
            'CARS-T' => [853, 'Central Asian Russian Scriptures (CARS-Tajikistan)'],
            'CASS70' => [480, 'перевод Еп. Кассиана'],
            'CSLAV' => [45, 'Библия на церковнославянском языке'],
            'RSP' => [201, 'Святая Библия: Современный перевод'],
            'SYNO' => [400, 'Синодальный перевод'],
            'НРП' => [143, 'Новый Русский Перевод'],
            'СИНОД' => [167, 'Синодальный перевод'],
            'СРП-2' => [1999, 'Современный русский перевод (2-е изд.)'],
        ],

        // Українська (Ukrainian)
        'uk' => [
            'UBIO' => [186, 'Біблія в пер. Івана Огієнка 1962'],
            'UKRK' => [188, 'Біблія в пер. П.Куліша та І.Пулюя, 1905'],
            'UMT' => [204, 'Свята Біблія: Сучасною мовою'],
            'НПУ' => [3269, 'Новий Переклад Українською'],
            'НУП' => [3149, 'Переклад. Ю. Попченка.'],
            'УТТ' => [1755, 'Переклад Р. Турконяка'],
        ],
    ];

    private static array $defaultTranslations = [
        'en' => 'NIV',
        'es' => 'RVR1960',
        'fr' => 'LSG',
        'pt' => 'NVI',
        'de' => 'SCH2000',
        'ru' => 'НРП',
        'uk' => 'UMT',
    ];

    private static array $alternatives = [
        // English
        'NIV2011' => 'NIV',
        'NASB' => 'NASB2020',

        // Deutsch (German)
        'NGU' => 'NGU2011',
        'SCH' => 'SCH2000',
        'S2000' => 'SCH2000',

        // Español (Spanish)
        'RVR' => 'RVR1960',

        // Français (French)
        'SG21' => 'S21',
        'SG1910' => 'LSG',
        'OSTERVALD' => 'OST',
        'PDV' => 'PDV2017',
        'DAR' => 'FRDBY',
        'NEG1979' => 'NEG79',

        // Русский (Russian)
        'NRT' => 'НРП',
        'RST' => 'SYNO',
    ];

    public static function isLanguageSupported(string $languageId) : bool {
        return isset(Self::$defaultTranslations[$languageId]) && isset(Self::$translations[$languageId]);
    }

    /**
     * Returns a string which is an alternative translation code, or null if not found
     */
    public static function alternativeTranslation(string $translationAbbreviation): ?string {
        if (isset(Self::$alternatives[$translationAbbreviation]))
            return Self::$alternatives[$translationAbbreviation];

        return null;
    }

    public static function getDefaultTranslationId(string $languageId): string {
        return Self::$defaultTranslations[$languageId];
    }

    /**
     * Returns a string which is the translation ID that bible.com expects, or null if not found
     */
    public static function getTranslationId(string $languageId, string $translationAbbreviation): ?string {
        return Self::get($languageId, $translationAbbreviation)[0] ?? null;
    }

    /**
     * Returns an array of two items: 0) the bible.com ID and 1) the name, or null if not found
     */
    public static function get(string $languageId, string $translationAbbreviation): ?array {
        return Self::$translations[$languageId][$translationAbbreviation] ?? null;
    }
}
