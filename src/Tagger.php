<?php

namespace ChurchRenewal\BibleLinker;

/**
 * This class handles removing html tags from a string (so that the plain text can be parsed easier),
 * in such a way that inserting and deleting the plain text characters can be backward applied
 * to the original html.
 */

/*
 * Example:
 *
 * This HTML: <strong>John</strong> <strong>3:16</strong>
 *
 * gets parsed into this plain text: John 3:16
 *
 * and this map gets created that maps each position in the plain text to its position in the html string
 *
 * [8, 9, 10, 11, 21, 29, 30, 31, 32, 33]
 *
 *  8 through 11 are "John"
 *  29 is space
 *  30 through 33 are "3:16"
 *
 */

class Tagger {
	/**
	 * each element in $map is an associative array with keys
	 *   'fromHTML'
	 *   'toHTML'
	 *   'fromText'
	 *   'toText'
	 */
	private array $map = [];
	private string $html = "";
	private string $plainText = "";

	private const BLOCK_LEVEL_ELEMENTS = [
		"address",
		"article",
		"aside",
		"blockquote",
		"canvas",
		"dd",
		"div",
		"dl",
		"dt",
		"fieldset",
		"figcaption",
		"figure",
		"footer",
		"form",
		"h1",
		"h2",
		"h3",
		"h4",
		"h5",
		"h6",
		"header",
		"hr",
		"li",
		"main",
		"nav",
		"noscript",
		"ol",
		"p",
		"pre",
		"section",
		"table",
		"td",
		"th",
		"tfoot",
		"tr",
		"ul",
		"video",
	];

	/* We insert this character into the plain text between block-level tags,
	 * so that when looking for references, we won't wander from one block-level
	 * tag to another.
	 * e.g. <p>John 3</p><p>:16</p> should not match John 3:16,
	 * but <span>John 3</span><span>:16</span> should.
	 */
	// private const BOUNDARY_CHARACTER = "\07";
	private const BOUNDARY_CHARACTER = "|";

	public function __construct($html) {
		$this->html = $html;
		$this->createMap();
	}

	public function getMap(): array {
		return $this->map;
	}

	public function getPlainText(): string {
		return $this->plainText;
	}

	public function getHtml(): string {
		return $this->html;
	}

	private function createMap(): void {
		$this->plainText = "";

		$inTag = false;
		$tagName = "";
		$gatheringTag = false;

		$foundTag = function ($tagName, $htmlPos) {
			if ($tagName != "" && $tagName[0] == "/") {
				$tagName = substr($tagName, 1);
			}

			if (!in_array($tagName, self::BLOCK_LEVEL_ELEMENTS)) {
				return; // it's an inline tag, so we don't care
			}

			$this->plainText .= self::BOUNDARY_CHARACTER;
			$this->map[strlen($this->plainText) - 1] = $htmlPos;
		};

		for ($htmlPos = 0; $htmlPos < strlen($this->html); $htmlPos++) {
			$char = $this->html[$htmlPos];

			if ($char == "<") {
				$inTag = true;
				$tagName = "";
				$gatheringTag = true;
			} elseif ($char == ">") {
				if ($gatheringTag) {
					$foundTag($tagName, $htmlPos);
				}
				$gatheringTag = false;
				$inTag = false;
			} elseif ($inTag) {
				if ($char == " ") {
					if ($gatheringTag) {
						$foundTag($tagName, $htmlPos);
					}
					$gatheringTag = false;
				}
				if ($gatheringTag) {
					$tagName .= $char;
				}
			} else {
				// between tags
				$this->plainText .= $char;
				$this->map[strlen($this->plainText) - 1] = $htmlPos;
			}
		}
	}

	private function createMapWithoutBoundaryCharacter(): void {
		$this->plainText = "";

		$inTag = false;

		for ($htmlPos = 0; $htmlPos < strlen($this->html); $htmlPos++) {
			$char = $this->html[$htmlPos];

			if ($char == "<") {
				$inTag = true;
			} elseif ($char == ">") {
				$inTag = false;
			} elseif (!$inTag) {
				$this->plainText .= $char;
				$this->map[strlen($this->plainText) - 1] = $htmlPos;
			}
		}
	}

	/**
	 * Returns true if any character in the given range is in an <a> tag
	 */
	public function isInATag(int $start, int $length): bool {
		for ($i = 0; $i < $length; $i++) {
			if ($this->isCharInTag($start + $i)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns true if the given character is in an <a> tag
	 */
	private function isCharInTag(int $position): bool {
		$htmlPos = $this->map[$position];

		// a character is inside an "a" tag iff there is a "</a>" tag after it and there is no "<a" tag before that one

		$posACloseTag = strpos($this->html, "</a>", $htmlPos);
		$posAOpenTag = strpos($this->html, "<a", $htmlPos);

		if ($posACloseTag === false) {
			// There is no closing </a> tag after this point, so we can't be in an <a> tag.
			return false;
		}

		if ($posAOpenTag === false) {
			// Remember that $posACloseTag was found, but there is no opening <a> tag in between, so we are in an <a> tag.
			return true;
		}

		if ($posAOpenTag < $posACloseTag) {
			// The opening tag is nearer than the closing, so neither of them apply to us.  We're not in that range.
			return false;
		}

		return true;
	}

	public function surroundWithTags(int $start, int $length, string $openTag, string $closeTag): void {
		$this->insertBefore($start + $length, $closeTag);
		$this->insertAfter($start, $openTag);
		$this->createMap();
	}

	/**
	 * The $start is the beginning of the text to be surrounded with an A tag.  Maybe the A tag only starts part way through.
	 */
	public function removeATag(int $start): void {
		$closeATagStart = strpos($this->html, "</a>", $this->map[$start]);
		$closeATagEnd = $closeATagStart + 4;

		$openATagStart = strrpos(substr($this->html, 0, $closeATagStart), "<a");
		$openATagEnd = strpos($this->html, ">", $openATagStart) + 1;

		// remove the closing tag
		$this->html = substr($this->html, 0, $closeATagStart) . substr($this->html, $closeATagEnd);

		// remove the opening tag
		$this->html = substr($this->html, 0, $openATagStart) . substr($this->html, $openATagEnd);

		$this->createMap();
	}

	/**
	 * Make sure to run createMap() after calling this function
	 */
	private function insertBefore(int $position, string $htmlString): void {
		if ($position == 0) {
			$this->html = substr_replace($this->html, $htmlString, $this->map[0], 0);
		} else {
			$this->html = substr_replace($this->html, $htmlString, $this->map[$position - 1] + 1, 0);
		}
	}

	/**
	 * Make sure to run createMap() after calling this function
	 */
	private function insertAfter(int $position, string $htmlString): void {
		$this->html = substr_replace($this->html, $htmlString, $this->map[$position], 0);
	}

	/**
	 * Make sure to run createMap() after calling this function
	 */
	private function delete(int $position, int $length): void {
		for ($i = $position + $length - 1; $i >= $position; $i--) {
			// We will delete one character at a time starting from the end
			$this->html = substr_replace($this->html, "", $this->map[$i], 1);
		}
	}

	/**
	 * This function makes sure there are no HTML tags in this range (plain text range)
	 */
	public function detagify(int $start, int $length): void {
		$string = substr($this->plainText, $start, $length);

		$this->delete($start, strlen($string));
		$this->insertAfter($start, $string);

		$this->createMap();
	}
}
