#!/bin/bash

docker run --rm -it -v `pwd`:/app composer composer install &&
docker run --rm -it -v `pwd`:/app jitesoft/phpunit
