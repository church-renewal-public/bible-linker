# Bible Linker

Parses an HTML-encoded string searching for Bible references and replaces them with links to Bible.com.

## Features

1. Link to an individual verse
    ```php
    BibleLinker::linkify("Psalm 119:105", "en");
    /*
     * <a href="https://www.bible.com/bible/111/PSA.119.105.NIV">Psalm 119:105</a>
     */
    ```
1. Link to an entire chapter
    ```php
    BibleLinker::linkify("John 3", "en");
    /*
     * <a href="https://www.bible.com/bible/111/JHN.3..NIV">John 3</a>
     */
    ```
1. Link to a range of verses
    ```php
    BibleLinker::linkify("John 3:15-17", "en");
    /*
     * <a href="https://www.bible.com/bible/111/JHN.3.15-17.NIV">John 3:15-17</a>
     */
    ```
1. Link to multiple verses
    ```php
    BibleLinker::linkify("John 3:16, 18", "en");
    /*
     * <a href="https://www.bible.com/bible/111/JHN.3.16.NIV">John 3:16</a>,
     * <a href="https://www.bible.com/bible/111/JHN.3.18.NIV">18</a>
     */
    ```
1. Specify a translation
    ```php
    BibleLinker::linkify("John 3:16 (ESV)", "en");
    /*
     * <a href="https://www.bible.com/bible/59/JHN.3.16.ESV">John 3:16</a> (ESV)
     */
    ```
1. Abbreviate the book name with common abbreviations
    ```php
    BibleLinker::linkify("Genesis 1:1", "en");
    BibleLinker::linkify("Gen 1:1", "en");
    BibleLinker::linkify("Ge 1:1", "en");
    BibleLinker::linkify("Gn 1:1", "en");
    /*
     * <a href="https://www.bible.com/bible/111/GEN.1.1.NIV">Genesis 1:1</a>
     * <a href="https://www.bible.com/bible/111/GEN.1.1.NIV">Gen 1:1</a>
     * <a href="https://www.bible.com/bible/111/GEN.1.1.NIV">Ge 1:1</a>
     * <a href="https://www.bible.com/bible/111/GEN.1.1.NIV">Gn 1:1</a>
     */
    ```
1. And combinations of all the above
    ```php
    BibleLinker::linkify("Ge 1:1,2-3; 4; 5:6,7,8; 9:10 (NLT)", "en");
    /*
     * <a href="https://www.bible.com/bible/116/GEN.1.1.NLT">Ge 1:1</a>,
     * <a href="https://www.bible.com/bible/116/GEN.1.2-3.NLT">2-3</a>;
     * <a href="https://www.bible.com/bible/116/GEN.4..NLT">4</a>;
     * <a href="https://www.bible.com/bible/116/GEN.5.6.NLT">5:6</a>,
     * <a href="https://www.bible.com/bible/116/GEN.5.7.NLT">7</a>,
     * <a href="https://www.bible.com/bible/116/GEN.5.8.NLT">8</a>;
     * <a href="https://www.bible.com/bible/116/GEN.9.10.NLT">9:10</a> (NLT)
     */
    ```
1. Single chapter books don't require chapter
    ```php
    BibleLinker::linkify("3 John 4", "en");
    BibleLinker::linkify("3 John 1:4", "en");
    /*
     * <a href="https://www.bible.com/bible/111/3JN.1.4.NIV">3 John 4</a>
     * <a href="https://www.bible.com/bible/111/3JN.1.4.NIV">3 John 1:4</a>
     */
    ```
1. Non-existant chapters and verse do not get linked
    ```php
    BibleLinker::linkify("Psalm 150; 151", "en"); // Psalm 151 doesn't exist
    BibleLinker::linkify("3 John 1-16", "en"); // 3 John 15-16 don't exist
    /*
     * <a href="https://www.bible.com/bible/111/PSA.150..NIV">Psalm 150</a>; 151
     * <a href="https://www.bible.com/bible/111/3JN.1.1-14.NIV">3 John 1-16</a>
     */
    ```
1. Inline tags are tolerated
    ```php
    BibleLinker::linkify("<span>John</span> 15:<span style='color:blue'>5</span>-6", "en"); // John 15:5-6
    /*
     * <span><a href="https://www.bible.com/bible/111/JHN.15.5-6.NIV">John 15:5-6</a></span><span style="color:blue"></span>
     */
    ```
1. Block-level tags are not respected like inline tags are
    ```php
    BibleLinker::linkify("<p>Review John 15</p><p>:5</p>", "en"); // John 15 (not John 15:5)
    /*
     * <p>Review <a href="https://www.bible.com/bible/111/JHN.15..NIV">John 15</a></p><p>:5</p>
     */
    ```

## Supported Languages

1. English (default translation: NIV)
2. Spanish (default translation: RVR1960)
3. French (default translation: LSG)
4. Portuguese (default translation: NVI)

## Including In Your Project

1. run `composer require church-renewal/bible-linker`

## Usage Examples

```php
use ChurchRenewal\BibleLinker\BibleLinker;

/**
 * @param string - a two character language ID
 * @return bool - true if language is supported by Bible Linker
 */
BibleLinker::isLanguageSupported('es')
/*
 * true
 */

/**
 * @param string - must be HTML encoded
 * @param string - a two character language ID
 * @param array - optional: associative array of attributes to be added to the A tag
 * @return array - assocative array with keys:
 *     'new':                   int - number of new links inserted into content
 *     'alternative':           int - number of translations found in content which couldn't be found, but have known alternatives
 *     'unknown':               int - number translations found in content which couldn't be found on Bible.com
 *     'unknownTranslations':   array - an array of strings of the unknown translations
 *     'html':                  string - the content with links inserted
 *     'errors' :               array - an associative array of parsing errors; each key is the string that failed, each value is the error code
 */
BibleLinker::linkify('Psalm 119:105', 'en', ['class' => 'verse', 'target' => '_blank']);
/*
 * [
 *     'new' => 1
 *     'alternative' => 0
 *     'unknown' => 0
 *     'unknownTranslations' => []
 *     'html' => '<a href="https://www.bible.com/bible/111/PSA.119.105.NIV" class="verse" target="_blank">Psalm 119:105</a>',
 *     'errors' => []
 * ]
 */
```

## Known Limitations

1. HTML content must be HTML-encoded. E.g. output from something like TinyMCE will work fine.

    ```php
    // good
    BibleLinker::linkify(htmlentities("Hébreux 12:2"), "fr");
    BibleLinker::linkify("H&eacute;breux 12:2", "fr");

    // bad
    BibleLinker::linkify("Hébreux 12:2", "fr");
    ```

1. Verses must be separated by commas

    ```php
    // good
    BibleLinker::linkify("Psalm 119:105, 106", "en");

    // bad
    BibleLinker::linkify("Psalm 119:105 & 106", "en");
    ```

1. Chapters must be separated by semi-colons

    ```php
    // good
    BibleLinker::linkify("Psalm 119; 120", "en");

    // bad
    BibleLinker::linkify("Psalm 119, 120", "en");
    ```

1. Translations must be surrounded by parentheses

    ```php
    // good
    BibleLinker::linkify("Psalm 119:105 (ESV)", "en");

    // bad
    BibleLinker::linkify("Psalm 119:105 ESV", "en");
    ```

1. Cannot specify more than one translation

    ```php
    // good
    BibleLinker::linkify("Psalm 118 (NASB); Psalm 119:105 (ESV)", "en");

    // bad
    BibleLinker::linkify("Psalm 118 (NASB); 119:105 (ESV)", "en");
    ```

## Running Tests In Development

Install Docker

```sh
./runtests.sh
```
